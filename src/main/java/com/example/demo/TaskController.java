package com.example.demo;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TaskController {
	
	@Autowired
	private TaskService TS;
	
	@RequestMapping("/hello")
	public String Hello() {
		return TS.hello();
	}
	
	@PostMapping("/newtask")
	public void AddTask(@RequestBody Task task) {
	
	      TS.AddTask(task);
	}
	
	@RequestMapping("/AllTasks")
	public List<Task> AllTasks(){
		return TS.TaskList();
	}
	
	@DeleteMapping("/delete/{id}")
	public void DeleteTasks(@PathVariable("id") int TaskId){
		System.out.println("in delete");
		TS.DeleteTask(TaskId);
	}
	
	@RequestMapping("/deletedtasks")
	public List<Task>DeletedTasks(){
		return TS.DeletedTasks();
		
	}
	
	@PutMapping("/comp/{id}")
	public void UpdateCompletion(@PathVariable int TaskId) {
		TS.MarkComplete(TaskId);
	}
	
	
	@RequestMapping("/complete")
	public List<Task> CompleteTasks(){
		return TS.CompleteTasks();
	}
	
	@RequestMapping("/incomplete")
	public List<Task> InCompleteTasks(){
		return TS.InCompleteTasks();
	}

}
