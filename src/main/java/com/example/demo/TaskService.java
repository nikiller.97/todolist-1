package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
public class TaskService {
	
	@Autowired
	private TaskRepository TR;
	
	public String hello() { return "hey Service"; }
	
	 public List<Task> TaskList() {
	    	List<Task> TodoList = new ArrayList<>();
	    	List<Task> TodoList1 = new ArrayList<>();
	    	TR.findAll().forEach(TodoList::add);
	    	for(Task task:TodoList) {
	    		if(task.isStatus())
	    		   TodoList1.add(task);
	    	}
	    	
	    	
	    	return TodoList1;
	    }
	
	public void AddTask(Task task) {
			
		TR.save(task);
	}
	
	public void MarkComplete(int TaskId) {
		Task CompTask = TR.findById(TaskId).get();
		if(CompTask.isStatus())
		CompTask.setCompletion(true);
	}
	
	public void DeleteTask(int TaskId) {
		Task SoftDelTask = TR.findById(TaskId).get();
		SoftDelTask.setStatus(false);
		TR.save(SoftDelTask);
		}
	


	public List<Task> CompleteTasks(){
		List<Task> CompTodoList1 = new ArrayList<>();
		List<Task> CompTodoList = new ArrayList<>();
		TR.findAll().forEach(CompTodoList1::add);
		for(Task task:CompTodoList1) {
			if(task.isCompletion())
				CompTodoList.add(task);
		}
		
		return CompTodoList;
	}

	
	public List<Task> InCompleteTasks(){
		List<Task> InCompTodoList1 = new ArrayList<>();
		List<Task> InCompTodoList = new ArrayList<>();
		TR.findAll().forEach(InCompTodoList1::add);
		for(Task task:InCompTodoList1) {
			if(!task.isCompletion())
				InCompTodoList.add(task);
		}
		
		return InCompTodoList;
	}
	
	public List<Task> DeletedTasks(){
		List<Task> DelList1 = new ArrayList<>();
		List<Task> DelList = new ArrayList<>();
		TR.findAll().forEach(DelList1::add);
		for(Task task:DelList1) {
			if(!task.isStatus())
				DelList.add(task);
		}
		
		return DelList;
	}

}
