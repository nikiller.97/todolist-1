package com.example.demo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
public class Task  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int TaskId;
	private String title;
    private String body;
    private boolean completion;
    private boolean status;
	
	public int getTaskId() {
		return TaskId;
	}

	public void setTaskId(int TaskId) {
		this.TaskId = TaskId;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	Task(){
		completion = false;
		status = true;
	}
	



	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}


	public boolean isCompletion() {
		return completion;
	}

	public void setCompletion(boolean completion) {
		this.completion = completion;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	

}
